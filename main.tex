\documentclass{marticl}
\usepackage{amsmath}

\usepackage{geometry}
	\geometry{margin = 3cm}

\usepackage{float}

\title{Composition Series and Jordan-Holder}
\author{Dennis Chen}

\begin{document}

\maketitle

\begin{abstract}
	The Jordan-Holder Theorem states, more or less, that every finite group factors uniquely into simple groups (i.e. groups with no non-trivial proper normal subgroups). Furthermore, any infinite group that factors into simple groups must also factor uniquely.
\end{abstract}

\section{Preliminaries}

I assume you are familiar with
\begin{enumerate}
	\item the axioms of group theory,
	\item what a normal subgroup is, and that $H, K \lhd G$ implies $HK \lhd G$,
	\item what a coset is,
	\item what a quotient group is,
	\item and the First, Second, and Fourth Isomorphism Theorems.
\end{enumerate}
(This list is intentionally constructed in an order that makes it convenient to review these topics.)

As a reminder, here are the Second and Fourth Isomorphism Theorems.

\begin{theo}[Second Isomorphism Theorem]
	Let $G$ be a group, $H \leq G$, and $N \leq N_G(H)$. Then $NH/H \cong N/N \cap H$, where all subgroups and quotients are well-defined.
\end{theo}

\begin{theo}[Fourth Isomorphism Theorem]
	Let $G$ be a group and $N \unlhd G$. Then there is a bijection
	\[f \colon \{H \mid N \leq H \leq G\} \to \{Q \mid Q \leq G/N\} \]
	defined as $f(H) = H/N$. Furthermore,
	\begin{enumerate}
		\item $H_1 \leq H_2 \iff H_1/N \leq H_2/N$
		\item $H \unlhd G \iff H/N \unlhd G/N$.
	\end{enumerate}
	So $f$ preserves subgroups and normality of subgroups of $G$.
\end{theo}

There are a garden variety of other properties that this bijection satisfies. None of them are important for our purposes here.

Now onto new content. We first start by defining a simple group and a composition series of a group.

\begin{defi}[Simple Groups]
	A group $G$ is simple if it has exactly two normal subgroups: $1$ and $G$.
\end{defi}

Another way to think of this definition is that $G$ has no interesting normal subgroups. \emph{Note 1 is not a simple group as it only has one normal subgroup.}

\begin{defi}[Composition Series]
	A composition series of $G$ is a \emph{finite} sequence of subgroups
	\[1 = G_0 \lhd G_1 \lhd \cdots \lhd G_n = G\]
	where $G_{i+1}/G_i$ is simple for all $0 \leq i < n$.

	We refer to $G_{i+1}/G_i$ as a \emph{composition factor} of $G$.
\end{defi}

Here is an example: take $Z_6$ (the cyclic group of order $6$). The only two composition series are
\[1 \lhd Z_2 \lhd Z_6\]
and
\[1 \lhd Z_3 \lhd Z_6.\]
(What are the quotient groups of successive subgroups in each composition series?)

An important fact is that all finite groups have a composition series. The argument goes like this: suppose we know that all groups with order less than $n$ have a composition series. Then take a group $G$ with $|G| = n$. Either $G$ is simple, in which case we are done ($1 \lhd G$), or it has an interesting normal subgroup $H$.

In the latter case, take a composition series
\[1 = H_0 \lhd \cdots \lhd H_n = H\]
and a composition series
\[H/H = S_0/H \lhd \cdots \lhd S_m/H = G/H\]
But note that by the Fourth Isomorphism Theorem,
\[1 = H_0 \lhd \cdots \lhd H \lhd S_1 \lhd \cdots \lhd S_m = G\]
is a composition series of $G$.

This fact does not directly relate to the statement of the Jordan-Holder Theorem. Yet it illuminates why we care: composition series are ubiquitous in finite group theory.

\section{Jordan-Holder}

Informally, Jordan-Holder states that any two composition series have the same length, and that their composition factors are the same (up to isomorphism and rearrangement).

\begin{theo}[Jordan-Holder]
	Suppose $G$ has two composition series
	\[1 = G_0 \lhd \cdots \lhd G_n = G\]
	and
	\[1 = H_0 \lhd \cdots \lhd H_m = H.\]
	Then there is a bijection $\pi \colon \{1, \ldots, n\} \to \{1, \ldots, m\}$ such that
	\[G_{\pi(i)}/G_{\pi(i) - 1} = H_{i}/H_{i-1}\]
	for all $0 \leq i < n$.
\end{theo}

The existence of a bijection implies $n = m$.

\subsection{Outline}

\subsubsection{Strategy}

We will utilize strong induction.

Take the smallest $k$ such that $H_k$ is not a subgroup of $G_{n-1}$. Here is our strategy: we are going to consider the series
\[1 = H_0 \cap G_{n-1} \unlhd \cdots \unlhd H_m \cap G_{n-1} = G_{n-1}.\]
This isn't a composition series of $G_{n-1}$ (in fact, it better not be, because it has a length of $m$ while we know $1 \lhd \cdots \lhd G_{n-1}$ has length $n-1$). But we will show that removing $H_k \cap G_{n-1}$ makes it one.

\subsubsection{The repeat}

We make the crucial observation that $H_k \cap G_{n-1} = H_{k-1}$. This is because $H_{k-1}$ and $H_k \cap G_{n-1}$ are both normal subgroups of $H_k$, so $H_{k-1}(H_k \cap G_{n-1}) \lhd H_k$ too. Thus
\[\frac{H_{k-1}(H_k \cap G_{n-1})}{H_{k-1}} \lhd \frac{H_k}{H_{k-1}}.\]
Note $\frac{H_k}{H_{k-1}}$ is simple and $H_{k-1}(H_k \cap G_{n-1})$ is a subgroup of $G_{n-1}$, so it cannot be equal to $H_k$. So $H_k \cap G_{n-1} = H_{k-1}$.

\subsubsection{Using inductions to establish most isomorphisms}

Now we are going to consider the composition series
\[(H_0 \cap G_{n-1}) \lhd \cdots \lhd (H_{k-1} \cap G_{n-1}) \lhd (H_{k+1} \cap G_{n-1}) \lhd \cdots \lhd (H_m \cap G_{n-1})\]
of $G_{n-1}$ (note that we have removed $H_k \cap G_{n-1}$)
which has composition factors
\[\frac{H_1 \cap G_{n-1}}{H_0 \cap G_{n-1}}, \ldots, \frac{H_{k-1} \cap G_{n-1}}{H_{k-2} \cap G_{n-1}}, \frac{H_{k+1} \cap G_{n-1}}{H_k \cap G_{n-1}}, \ldots, \frac{H_m \cap G_{n-1}}{H_{m-1} \cap G_{n-1}}.\]
Then we show it is isomorphic to the sequence of composition factors
\[\frac{H_1}{H_0}, \ldots, \frac{H_{k-1}}{H_{k-2}}, \frac{H_{k+1}}{H_k}, \ldots, \frac{H_m}{H_{m-1}}.\]

But also, as $H_0 \cap G_{n-1} \lhd \cdots \lhd H_m \cap G_{n-1}$ is a composition series of $G_{n-1}$, its composition factors must also be isomorphic to the composition factors of
\[G_0 \lhd \cdots \lhd G_{n-1}.\]

\subsubsection{The Final Correspondence}

So we have set up a correspondence between most of the composition factors of $G_0 \lhd \cdots \lhd G_n$ and $H_0 \lhd \cdots \lhd H_m$. One pair remains: we need to show
\[\frac{G}{G_{n-1}} \cong \frac{H_k}{H_{k-1}}.\]
This is true as
\[\frac{G}{G_{n-1}} = \frac{H_kG_{n-1}}{G_{n-1}} \cong \frac{H_k}{H_k \cap G_{n-1}} = \frac{H_k}{H_{k-1}}.\]

\subsection{Proof}

We utilize strong induction on $\max(n, m)$.

Take the smallest $k$ such that $H_k$ is not a subgroup of $G_{n-1}$.

\begin{lemma}
	\label{lemma:repeat}
	$H_k \cap G_{n-1} = H_{k-1}$.
\end{lemma}

\begin{pro}
	Note that $H_{k-1} \lhd H_k$ by definition and $H_k \cap G_{n-1} \lhd H_k$ as $G_{n-1} \lhd G$.\footnote{This relies on the following fact: $H \leq G$ and $N \lhd G$ implies $H \cap N \lhd H$. Proof: suppose $n \in H \cap N$. Then for all $h \in H$, $hnh^{-1} \in N$ as $N$ is normal, and $hnh^{-1} \in H$ as $n \in H$. So elements in $H \cap N$ are closed under conjugation by elements in $H$, as desired.}

	Obviously $H_{k-1} \subseteq H_{k-1}(H_k \cap G_{n-1})$. Combining this with the well-known fact that $H, K \lhd G$ implies $HK \lhd G$, we note that
	\[H_{k-1} \lhd H_{k-1}(H_k \cap G_{n-1}) \lhd H_k.\footnote{Since $H_{k-1}$ is a normal subgroup of $H_k$, it must be a normal subgroup of any subgroup of $H_k$ containing $H_{k-1}$. Since $H_k \cap G_{n-1}$ is such a subgroup, we must have $H_{k-1} \lhd H_k \cap G_{n-1}$.}\]
	But $\frac{H_k}{H_{k-1}}$ is simple, so by the Fourth Isomorphism Theorem, we must either have
	\[H_{k-1} = H_{k-1}(H_k \cap G_{n-1}) \text{ or } H_k = H_{k-1}(H_k \cap G_{n-1}).\]
	The latter is impossible since $G_{n-1} \leq H_{k-1}(H_k \cap G_{n-1})$ yet $G_{n-1} \not\leq H_k$. So we have
	\[H_{k-1} = H_{k-1}(H_k \cap G_{n-1}).\]

	This implies $H_k \cap G_{n-1} \leq H_{k-1}$. But as $H_{k-1} \leq H_k$ and $H_{k-1} \leq G_{n-1}$, we also have $H_{k-1} \leq H_k \cap G_{n-1}$. This establishes a double inclusion, so $H_k \cap G_{n-1} = H_{k-1}$, as desired.
\end{pro}

\begin{lemma}
	\label{lemma:subnormal}
	Consider the series
	\[\frac{H_0G_{n-1}}{G_{n-1}}, \ldots, \frac{H_mG_{n-1}}{G_{n-1}}.\]
	If $0 \leq i < k$ then
	\[\frac{H_iG_{n-1}}{G_{n-1}} \cong 1,\]
	and if $k \leq i \leq m$ then
	\[\frac{H_iG_{n-1}}{G_{n-1}} = \frac{G}{G_{n-1}}.\]
\end{lemma}

\begin{pro}
	Let $A$ and $B$ be subgroups of $G$, and $N$ be a normal subgroup of $G$. Recall that $A \lhd B$ implies $\frac{AN}{N} \lhd \frac{BN}{N}$.\footnote{Proof: $(bN)(aN)(b^{-1}N) = bab^{-1}N$, and we know $bab^{-1} \in A$ so $bab^{-1}N \in AN$.} So
	\[H_0 \lhd \cdots \lhd H_m\]
	implies
	\[1 \cong \frac{H_0G_{n-1}}{G_{n-1}} \lhd \cdots \lhd \frac{H_mG_{n-1}}{G_{n-1}} = \frac{G}{G_{n-1}}.\]
	By the simplicity of $\frac{G}{G_{n-1}}$, each $\frac{H_iG_{n-1}}{G_{n-1}}$ is either isomorphic to $1$ or equivalent to $\frac{G_n}{G_{n-1}}$. If $H_i \leq G_{n-1}$ the former is true. If $H_i \not\leq G_{n-1}$ then the former cannot be true, so the latter is true.
\end{pro}

\begin{lemma}
	For all $i < k - 1$,
	\[\frac{H_{i+1} \cap G_{n-1}}{H_i \cap G_{n-1}} \cong \frac{H_{i+1}}{H_i}.\]
\end{lemma}

\begin{pro}
	This is obvious as $H_i, H_{i+1} \leq G_{n-1}$.
\end{pro}

\begin{lemma}
	For all $i \geq k$,
	\[\frac{H_{i+1} \cap G_{n-1}}{H_i \cap G_{n-1}} \cong \frac{H_{i+1}}{H_i}.\]
\end{lemma}

\begin{pro}
	We claim $H_{i}(H_{i+1} \cap G_{n-1}) = H_{i+1}$. It is a normal subgroup of $H_{i+1}$ as $H_{i} \lhd H_{i+1}$ and $H_{i+1} \cap G_{n-1} \lhd H_{i+1}$. Furthermore $H_{i}$ is a normal subgroup of it.

	By the Second Isomorphism Theorem and Lemma \ref{lemma:subnormal},
	\[\frac{G}{G_{n-1}} = \frac{H_{i+1}G_{n-1}}{G_{n-1}} \cong \frac{H_{i+1}}{H_{i+1} \cap G_{n-1}}.\]
	Note this quotient is simple.

	Since
	\[H_{i+1} \cap G_{n-1} \lhd H_{i}(H_{i+1} \cap G_{n-1}) \lhd H_{i+1},\footnote{Note $H_{i+1} \cap G_{n-1} \lhd H_{i+1}$ so $H_{i+1} \cap G_{n-1} \lhd H_i(H_{i+1} \cap G_{n-1})$.}\]
	simplicity of the quotient yields
	\[H_{i}(H_{i+1} \cap G_{n-1}) = H_{i+1} \cap G_{n-1} \text{ or } H_{i}(H_{i+1} \cap G_{n-1}) = H_{i+1}.\]
	Obviously the first case isn't true.\footnote{Proof: $G_{n-1} \not\leq H_i(H_{i+1} \cap G_{n-1})$ but $G_{n-1} \leq H_{i+1} \cap G_{n-1}$.} So it must be the second.

	Now by the Second Isomorphism Theorem,
	\[\frac{H_{i+1}}{H_i} = \frac{H_i(H_{i+1} \cap G_{n-1})}{H_i} \cong \frac{H_{i+1} \cap G_{n-1}}{H_i \cap (H_{i+1} \cap G_{n-1})} = \frac{H_{i+1} \cap G_{n-1}}{H_i \cap G_{n-1}},\]
	as desired. (We have $H_i \leq N_G(H_{i+1} \cap G_{n-1})$ as $H_i \lhd H_{i+1}$ and $H_{i+1} \cap G_{n-1} \leq H_{i+1}$.)
\end{pro}

Combining our lemmas, we now turn to the series
\[(H_0 \cap G_{n-1}) \lhd \cdots \lhd (H_{k-1} \cap G_{n-1}) \lhd (H_{k+1} \cap G_{n-1}) \lhd \cdots \lhd (H_m \cap G_{n-1}).\]
Note its composition factors are isomorphic to
\[\frac{H_1}{H_0}, \ldots, \frac{H_{k-1}}{H_{k-2}}, \frac{H_{k+1}}{H_k}, \ldots, \frac{H_m}{H_{m-1}}.\]
Because each of these quotients are simple, the series is a composition series.

Now we resolve one potential hiccup: note
\[
	\frac{H_{k+1} \cap G_{n-1}}{H_{k-1} \cap G_{n-1}}
	= \frac{H_{k+1} \cap G_{n-1}}{H_{k-1}}
	= \frac{H_{k+1} \cap G_{n-1}}{H_k \cap G_{n-1}}.
\]
So the $H_{k-1} \cap G_{n-1}$ term is performing double duty.

As
\[G_0 \lhd \cdots \lhd G_{n-1}\]
is a composition series of $G_{n-1}$, by the inductive hypothesis, there is some bijection
\[\pi_0 \colon \{1, \ldots, n-1\} \to \{1, \ldots, k-1, k+1, \ldots, m-1\}\]
such that
\[\frac{G_{\pi_0(i)}}{G_{\pi_0(i) - 1}} \cong \frac{H_i}{H_{i - 1}}\]
for all $1 \leq i \leq n-1$.

\begin{lemma}
	We have
	\[\frac{G}{G_{n-1}} \cong \frac{H_k}{H_{k-1}}.\]
\end{lemma}

\begin{pro}
	Note $G = G_{n-1}H_k$ by Lemma \ref{lemma:subnormal} as $G_{n-1}H_k \neq G_{n-1}$. So
	\[\frac{G}{G_{n-1}} = \frac{H_kG_{n-1}}{G_{n-1}},\]
	which is isomorphic to
	\[\frac{H_k}{H_k \cap G_{n-1}} = \frac{H_k}{H_{k-1}}\]
	by the Second Isomorphism Theorem. (The equivalence is a consequence of Lemma \ref{lemma:repeat}.)
\end{pro}

Now we extend $\pi_0$ to a bijection
\[\pi \colon \{1, \ldots, n\} \to \{1, \ldots, m\}\]
where
\[\frac{G_{\pi(i)}}{G_{\pi(i) - 1}} \cong \frac{H_i}{H_{i - 1}}\]
for all $1 \leq i \leq n$. Easy:

\[
	\pi(i) =
	\begin{cases}
		\pi_0(i) & 1 \leq i \leq n-1, \\
		k & i = n.
	\end{cases}
\]

\section{Does the converse hold?}

The factorization of a group $G$ into simple subgroups is unique, much as the factorization of a natural number $n$ into primes is unique. Furthermore, the prime factors of a natural number uniquely determine said number.

This gives rise to a natural question: do the composition factors of a group $G$ uniquely determine the group? In other words, does the converse of Jordan-Holder hold? The answer is no:
\[1 \lhd Z_p \lhd Z_{p^2}\]
and
\[1 \lhd Z_p \lhd Z_p \times Z_p.\]

The composition factors are identical (the only non-trivial check is $Z_{p^2}/Z_p \cong Z_p \cong (Z_p \times Z_p)/Z_p$) yet $Z_{p^2}$ and $Z_p \times Z_p$ are not isomorphic.

\end{document}
